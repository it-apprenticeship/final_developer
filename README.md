# IT Apprenticeship Quiz Frontend

## To-DOs
[*] PWA integration  
[*] Answer-Checkbox Component


## Setup
- Project setup  
`yarn install`

- Compiles and hot-reloads for development  
`yarn serve`

- Compiles and minifies for production  
`yarn build`

- Lints and fixes files  
`yarn lint`
