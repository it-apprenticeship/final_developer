import VueI18n from "vue-i18n";
import Vue from "vue";

const messages = {
  en: {
    global: {
      app: "Computer Scientists Quiz",
      next: "Next"
    },
    error: {
      notFound: "Page Not Found"
    }
  },
  de: {
    global: {
      app: "Fach",
      app2: "informatiker Quiz",
      next: "Weiter"
    },
    error: {
      notFound: "Seite nicht gefunden"
    }
  }
};

Vue.use(VueI18n);
export default new VueI18n({
  locale: "de",
  messages
});
