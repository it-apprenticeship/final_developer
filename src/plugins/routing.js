import Vue from "vue";
import VueRouter from "vue-router";
import StartSite from "../views/StartSite";
import QuestionSite from "../views/QuestionSite";
import NotFound from "../views/404";

Vue.use(VueRouter);

const routes = [
  { path: "/", name: "start", component: StartSite },
  { path: "/question/:id", name: "question", component: QuestionSite },
  { path: "*", name: "404", component: NotFound }
];

export default new VueRouter({
  routes
});
