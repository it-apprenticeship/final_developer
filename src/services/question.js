import api from "./api";

export function loadQuestions() {
  fetch(api.routes.questions)
    .then(response => {
      if (response.status === 200) {
        return response.json();
      } else {
        throw new Error("Something went wrong on api server!");
      }
    })
    .then(data => (questions = data))
    .catch(error => console.error(error));
}

export let questions = [];

let current;
export function randomQuestion() {
  const min = 0;
  const max = questions.length - 1;
  let random = current;
  do {
    random = (Math.random() * (max - min) + min).toFixed(0);
  } while (current == random);
  current = random;
  return questions[random];
}
