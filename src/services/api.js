const apiURL = "/api";
const headers = new Headers({
  "Content-Type": "application/json"
});

export default {
  url: apiURL,
  routes: {
    questions: new Request(`${apiURL}/questions`, { headers: headers, method: "GET" })
  }
};
