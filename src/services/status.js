export const STATUS = {
  BLANK: "BLANK",
  FILLED: "FILLED",
  SUCCESSFUL: "SUCCESSFUL",
  FAILURE: "FAILURE"
};

export function resolveStatus(value, answers) {
  const correctId = answers.data.find(answer => {
    return answer.correct === true;
  }).id;

  if (value == correctId) {
    return STATUS.SUCCESSFUL;
  } else {
    return STATUS.FAILURE;
  }
}
