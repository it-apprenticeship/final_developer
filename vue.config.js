module.exports = {
  pages: {
    index: {
      entry: "src/main.js",
      template: "public/index.html",
      filename: "index.html",
      title: "Informatics Test"
    }
  },
  devServer: {
    proxy: {
      "/api": {
        target: "http://localhost:9090",
        pathRewrite: { "^/api": "" }
      }
    }
  },
  pwa: {
    name: "Informatics Test",
    themeColor: "#28264a",
    msTileColor: "#000000",
    workboxPluginMode: "InjectManifest",
    workboxOptions: {
      swSrc: "public/serviceWorker.js"
    }
  }
};
